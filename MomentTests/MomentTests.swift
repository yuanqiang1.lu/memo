//
//  MomentTests.swift
//  MomentTests
//
//  Created by knight on 2019/9/25.
//  Copyright © 2019 Goswift ChengDu Co.,Ltd. All rights reserved.
//

import XCTest
import Moya
import NSObject_Rx


@testable import Moment

class MomentTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testNetworkStuffs() {
        let expectation = self.expectation(description: "the task should complete")
        LYQNetwork.request(MultiTarget(TWServiceApi.momenList))
            .mapArrayModel(LYQBaseModel.self)
            .subscribe(onNext: { (rs) in
                expectation.fulfill()
            }, onError: { (error) in
                print("error describtion:\(LYQErrorDescription(error)) -- code: \(KLCErrorCode(error))")
            }).disposed(by:rx.disposeBag)
        waitForExpectations(timeout: 10, handler: nil)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testBase64StringEcoding() {
        let testStr = "https://thoughtworks-mobile-2018.herokuapp.com/images/tweets/005.jpeg"
        let codeStr = "aHR0cHM6Ly90aG91Z2h0d29ya3MtbW9iaWxlLTIwMTguaGVyb2t1YXBwLmNvbS9pbWFnZXMvdHdlZXRzLzAwNS5qcGVn"
        let encodeStr = testStr.encode64()
        if let str = encodeStr {
            assert(codeStr == str, "is equal")
        }
        assert(encodeStr != nil, "is encode str exist")
        let decodeStr = encodeStr?.decode64()
        if let str = decodeStr {
            assert(str == testStr, "is equal")
        }
    }

}

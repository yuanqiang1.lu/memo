//
//  QuickTest.swift
//  MomentTests
//
//  Created by knight on 2019/10/5.
//  Copyright © 2019 Goswift ChengDu Co.,Ltd. All rights reserved.
//

import Foundation
import Moya
import NSObject_Rx
import Quick
import Nimble
@testable import Moment

class QuickTests: QuickSpec {

    override func spec() {

        describe( "Test network stuffs" ) {
            it("comments list") {
                let expectation = self.expectation(description: "the task should complete")
                LYQNetwork.request(MultiTarget(TWServiceApi.momenList))
                    .mapArrayModel(FriendMomentModel.self)
                    .subscribe(onNext: { (rs) in
                        guard let objList = rs else {
                            fail("rs is nil")
                            return
                        }
                        expect(objList.count == 22).to(beTrue())
                        if let momeArr = LYQMomeViewModel().convertMoment(modelArr: objList) {
                            expect(momeArr.count == 17).to(beTrue())
                        }
                        expectation.fulfill()
                    }, onError: { (error) in
                        print("error describtion:\(LYQErrorDescription(error)) -- code: \(KLCErrorCode(error))")
                    }).disposed(by:self.rx.disposeBag)
                self.waitForExpectations(timeout: 10, handler: nil)
            }

        }

        describe( "TestBase64StringEcoding" ) {
            it("it should be some stuffs") {
                //given / arrange
                let testStr = "https://thoughtworks-mobile-2018.herokuapp.com/images/tweets/005.jpeg"
                let codeStr = "aHR0cHM6Ly90aG91Z2h0d29ya3MtbW9iaWxlLTIwMTguaGVyb2t1YXBwLmNvbS9pbWFnZXMvdHdlZXRzLzAwNS5qcGVn"
                //when / act
                let encodeStr = testStr.encode64()
                if let str = encodeStr {
                    assert(codeStr == str, "is equal")
                }
                assert(encodeStr != nil, "is encode str exist")
                let decodeStr = encodeStr?.decode64()
                //then / assert
                if let str = decodeStr {
                    expect(str == testStr).to(beTrue())
                }

            }
        }

        describe( "Transform model to json string" ) {
            it("MomeCommentModel transform "){
                let comment = MomeCommentModel()
                comment.content = "It is something stuffs"
                let sender = UserModel()
                sender.avatar = "https://thoughtworks-mobile-2018.herokuapp.com/images/user/avatar.png"
                sender.nick = "ack"
                sender.username = "maker"
                comment.sender = sender

                guard let jsonStr = comment.toJSONString() else {
                    fail("• jsonStr is nil")
                    return
                }

                let obj = MomeCommentModel.deserialize(from: jsonStr)
                expect(obj?.content == comment.content ).to(beTrue())
                expect(obj?.sender?.avatar == sender.avatar).to(beTrue())
                expect(obj?.sender?.username == sender.username).to(beTrue())
            }
        }

        describe("Test load userInfo json") {
            it("Test userInfo json") {
                waitUntil { done in
                    if let path = Bundle.main.path(forResource: "userInfo", ofType: "json") {
                        let url = URL(fileURLWithPath: path)
                        do {
                            let jsonData = try Data(contentsOf: url)
                            if let jsonStr = String(data: jsonData, encoding: .utf8) {
                                let obj = UserInfoModel.deserialize(from: jsonStr)
                                let expectImgStr = "https://thoughtworks-mobile-2018.herokuapp.com/images/user/profile-image.jpg"
                                if let profileImg = obj?.profileImage {
                                    expect(profileImg).to(equal(expectImgStr))
                                } else {
                                    fail("• profileImg is not equal")
                                }
                            } else {
                                fail("• jsonStr  is nil")
                            }
                        } catch {
                            fail("• Catch exception")
                        }
                    }
                    Thread.sleep(forTimeInterval: 0.5)
                    done()
                }
            }
        }

    }
}

//
//  IMViewModelType.swift
//  InstallManage
//
//  Created by wang zhenqi on 2018/3/30.
//  Copyright © 2018年 Goswift iOS team All rights reserved.
//

import Foundation
public protocol LYQViewModelType {
    associatedtype LYQInput
    associatedtype LYQOutput
    
    /// 完成input和output的相互订阅
    func transform(input: LYQInput) -> LYQOutput
}

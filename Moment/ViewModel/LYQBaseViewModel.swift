//
//  IMBaseViewModel.swift
//  InstallManage
//
//  Created by knight on 2018/4/3.
//  Copyright © 2018年 Goswift iOS team All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

open class LYQBaseViewModel {
    public let disposeBag = DisposeBag()
    public init() {}
    
    // 监听网络状态,方便缺省页使用
    public var requestStatus = BehaviorRelay(value: KLCRequstResultStatus.requesting)
}

//
//  CommentViewCell.swift
//  Moment
//
//  Created by knight on 2019/9/26.
//  Copyright © 2019 Goswift ChengDu Co.,Ltd. All rights reserved.
//

import UIKit

class CommentViewCell: UITableViewCell {

    @IBOutlet weak var avatarImgView: UIImageView!

    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var commentLabel: UILabel!

    var model:MomeCommentModel? {
        didSet {
            guard let model = model else {
                return
            }

            commentLabel.text = model.content
            nameLabel.text = model.sender?.nick ?? ""
            if let avatarStr = model.sender?.avatar {
                ImgCacheManager.instance.imageForUrl(urlString: avatarStr ) { [unowned self] (img, url) in
                    self.avatarImgView.image = img
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

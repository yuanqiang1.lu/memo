//
//  MomeItemCell.swift
//  Moment
//
//  Created by knight on 2019/9/25.
//  Copyright © 2019 Goswift ChengDu Co.,Ltd. All rights reserved.
//

import UIKit
import SKPhotoBrowser

class MomeItemCell: UITableViewCell {

    @IBOutlet weak var avatarImgView: UIImageView!

    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var contentLabel: UILabel!

    @IBOutlet weak var imgCollectionView: UICollectionView!

    @IBOutlet weak var commentTbview: UITableView!

    @IBOutlet weak var imgCollectConstraintH: NSLayoutConstraint!

    @IBOutlet weak var commentTbConstaintH: NSLayoutConstraint!

    var imgModelArr:[MomeImg]?

    let defaultCommentCellHeight = 44

    lazy var frameImgView:UIImageView = {
        let imgView = UIImageView()
        imgView.image = UIImage(named: "avatar_mask")
        imgView.frame = avatarImgView.bounds
        return imgView
    }()

    /// To cache cell height
    var sizeDic: Dictionary<IndexPath, CGSize> = [:]

    var photoBrower:SKPhotoBrowser?

    lazy var coollectLayout:UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        return layout
    }()


    var model:FriendMomentModel? {
        didSet {
            guard let model = model else {
                return
            }
            nameLabel.text = model.sender?.nick ?? ""

            contentLabel.text = updateContentLabel(model: model)

            if let avatarUrl = model.sender?.avatar {
                ImgCacheManager.instance.imageForUrl(urlString:avatarUrl ) { [unowned self] (img, url) in
                    if let img = img {
                        self.avatarImgView.image = img
                    }
                }
            }

            imgModelArr = model.images
            let identifyStr = generateIdentifyStr()
            imgCollectionView.register(UINib(nibName: "MomeImgCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:identifyStr )
            let sizeTuple = updateCollectSize()
            imgCollectConstraintH.constant = sizeTuple.1

            imgCollectionView.reloadData()

            if let num = model.comments?.count {
                commentTbConstaintH.constant = CGFloat(defaultCommentCellHeight * num)
            } else {
                commentTbConstaintH.constant = 0
            }
            UIView.performWithoutAnimation {
                commentTbview.reloadData()
            }

        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        avatarImgView.addSubview(frameImgView)
        imgCollectionView.dataSource = self
        imgCollectionView.delegate = self
        self.imgCollectionView.collectionViewLayout = coollectLayout
        self.commentTbview.register(UINib(nibName: "CommentViewCell", bundle: nil), forCellReuseIdentifier: "CommentViewCell")
        self.commentTbview.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func updateContentLabel(model:FriendMomentModel) -> String {
        if let content = model.content {
            return content
        } else {
            if model.comments == nil && model.images == nil {
                return "No data,empty"
            }
            return ""
        }
    }

    func updateCollectSize() -> (CGSize,CGFloat) {
        let collectWidth = imgCollectionView.frame.width
        var sizeW = CGFloat(10)
        var sizeH = CGFloat(10)
        var collectH = CGFloat(20)
        if let num = imgModelArr?.count {
            if num == 1 {
                sizeW = (collectWidth - 2) / 2
                sizeH = (collectWidth - 2) / 2
                collectH = sizeH
                return (CGSize(width: sizeW,height: sizeH), collectH)
            }
            if num == 2 {
                sizeW = (collectWidth - 2) / 2
                sizeH = (collectWidth - 2) / 2
                collectH = sizeH
                return (CGSize(width: sizeW,height: sizeH), collectH)
            }

            if num == 3 {
                sizeW = (collectWidth - 2) / 3
                sizeH = (collectWidth - 2) / 3
                collectH = sizeH
                return (CGSize(width: sizeW,height: sizeH), collectH)
            }

            if num == 4 {
                sizeW = (collectWidth - 2) / 2
                sizeH = (collectWidth - 2) / 2
                collectH = sizeH * 2
                return (CGSize(width: sizeW,height: sizeH), collectH)
            }

            if num <= 6 {
                sizeW = (collectWidth - 2) / 3
                sizeH = (collectWidth - 2) / 3
                collectH = sizeH * 2
                return (CGSize(width: sizeW,height: sizeH), collectH)
            }

            if num > 6 {
                sizeW = (collectWidth - 2) / 3
                sizeH = (collectWidth - 2) / 3
                collectH = sizeH * 3
                return (CGSize(width: sizeW,height: sizeH), collectH)
            }

        }
        return (CGSize(width: sizeW,height: sizeH), collectH)
    }

    func photoBowerPrepare() {
        // Clean sk image array
        var skImgArr = [SKPhoto]()
        if let imgArr = imgModelArr {
            for m in imgArr {
                if let urlStr = m.url {
                    if let cachedImg = ImgCacheManager.instance.cachedImg(urlStr: urlStr) {
                        let photo = SKPhoto.photoWithImage(cachedImg)
                        skImgArr.append(photo)
                    } else {
                        let photo = SKPhoto.photoWithImageURL(urlStr)
                        photo.shouldCachePhotoURLImage = true
                        skImgArr.append(photo)
                    }
                }
            }
            if skImgArr.count > 0 {
                photoBrower = SKPhotoBrowser(photos: skImgArr)
            }
        }

    }

    func generateIdentifyStr() -> String {
        let modelIdentyStr = model?.identifyStr ?? ""
        if let num = imgModelArr?.count {
            var identifyStr = "MomeImgCollectionViewCell"
            if num <= 4 {
                identifyStr = "\(identifyStr)\(num)\(modelIdentyStr)"
            }
            if num <= 6 {
                identifyStr = "MomeImgCollectionViewCell5\(modelIdentyStr)"
            }

            if num > 6 {
                identifyStr = "MomeImgCollectionViewCell6\(modelIdentyStr)"
            }
            return identifyStr
        } else {
            return "MomeImgCollectionViewCell7"
        }
    }

    func deqeueCollectCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> MomeImgCollectionViewCell {
        let identifyStr = self.generateIdentifyStr()
        let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: identifyStr, for: indexPath) as! MomeImgCollectionViewCell
        if collectionCell.model != model?.images?[indexPath.row] {
            collectionCell.model = model?.images?[indexPath.row]
        }
        return collectionCell

    }
    
}

extension MomeItemCell :UICollectionViewDataSource,UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model?.images?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.deqeueCollectCell(collectionView, cellForItemAt: indexPath)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        photoBowerPrepare()
        if let brower = photoBrower, indexPath.row <= brower.photos.count {
            brower.initializePageIndex(indexPath.row)
            currentViewController?.present(brower, animated: true, completion: nil)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sizeTuple = updateCollectSize()
        return sizeTuple.0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
}

extension MomeItemCell : UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentViewCell") as! CommentViewCell

        cell.model = model?.comments?[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model?.comments?.count ?? 0
    }

}

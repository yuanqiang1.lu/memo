//
//  MomeImgCollectionViewCell.swift
//  Moment
//
//  Created by knight on 2019/9/26.
//  Copyright © 2019 Goswift ChengDu Co.,Ltd. All rights reserved.
//

import UIKit

class MomeImgCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgView: UIImageView!


    var model:MomeImg? {
        didSet {
            guard let model = model else {
                return
            }
            
            if let imgStr = model.url {
                ImgCacheManager.instance.imageForUrl(urlString: imgStr ) { [unowned self] (img, url) in
                    self.imgView.image = img
                }
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

}

//
//  FriendCommentModel.swift
//  Moment
//
//  Created by knight on 2019/10/5.
//  Copyright © 2019 Goswift ChengDu Co.,Ltd. All rights reserved.
//

import Foundation

class FriendMomentModel: LYQBaseModel,Equatable {

    var content:String?
    /// the user comment moment
    var sender:UserModel?
    var images:[MomeImg]?
    var comments:[MomeCommentModel]?
    /// Prepare for collection reuse identify
    var identifyStr:String?

    static func == (lhs: FriendMomentModel, rhs: FriendMomentModel) -> Bool {
        if lhs.sender == rhs.sender && lhs.content == rhs.content && lhs.comments == rhs.comments {
            return true
        }
        return false
    }
}

class MomeCommentModel: LYQBaseModel,Equatable {
    var content = ""
    var sender:UserModel?

    static func == (lhs: MomeCommentModel, rhs: MomeCommentModel) -> Bool {
        if lhs.sender == rhs.sender && lhs.content == rhs.content {
            return true
        }
        return false
    }
}

class MomeImg: LYQBaseModel,Equatable {
    static func == (lhs: MomeImg, rhs: MomeImg) -> Bool {
        if lhs.url == rhs.url {
            return true
        }
        return false
    }

    var url:String?
}

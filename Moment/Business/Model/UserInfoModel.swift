//
//  UserInfoModel.swift
//  Moment
//
//  Created by knight on 2019/9/25.
//  Copyright © 2019 Goswift ChengDu Co.,Ltd. All rights reserved.
//

import UIKit
import HandyJSON

class UserModel: LYQBaseModel,Equatable {

    var nick = ""
    var username = ""
    var avatar = ""

    static func == (lhs: UserModel, rhs: UserModel) -> Bool {
        if lhs.nick == rhs.nick && lhs.username == rhs.username && lhs.avatar == rhs.avatar {
            return true
        }
        return false
    }
}

class UserInfoModel: UserModel {
    var profileImage = ""

    override func mapping(mapper: HelpingMapper) {
        mapper <<<
            self.profileImage <-- "profile-image"
    }
}

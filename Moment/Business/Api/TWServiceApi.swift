//
//  KLCBookingApi.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/9/25.
//  Copyright © 2019 Goswift iOS Team. All rights reserved.
//

import Foundation
import Moya

enum TWServiceApi {
    case userInfo
    case momenList
}

extension TWServiceApi: TargetType {
    var baseURL: URL {
        switch self {
        case .userInfo,.momenList:
            return URL(string: MomentURL)!
        }
    }
    
    var path: String {
        switch self {
        case .userInfo:
            return "/user/jsmith"
        case .momenList:
            return "/user/jsmith/tweets"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .userInfo,.momenList:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String : String]? {
        return nil
    }
}

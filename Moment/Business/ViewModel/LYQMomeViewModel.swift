//
//  LYQMemoViewModel.swift
//  LXTBaseProduct
//
//  Created by knight on 2019/7/6.
//  Copyright © 2019 Goswift iOS team All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Moya

typealias SimpleBlock = () -> Void

class LYQMomeViewModel: LYQBaseViewModel, LYQViewModelType {

    let KeyMomentCache = "jsonstr"
    let KeyUserInfoCache = "userinfo"

    typealias LYQInput = LYQMomeInput
    typealias LYQOutput = LYQMomeOutput
    
    struct LYQMomeInput {
        let momelistCmd = PublishSubject<Void>()
        let userinfoCmd = PublishSubject<Void>()
    }
    
    struct LYQMomeOutput {
        let toastMsg = PublishSubject<(String, Double, SimpleBlock?)>()
        let userInfo = PublishSubject<UserInfoModel>()
        let listMomo = PublishSubject<[FriendMomentModel]>()
    }
    
    func transform(input: LYQMomeViewModel.LYQInput) -> LYQMomeViewModel.LYQOutput {
        let output = LYQOutput()
        userInfo(input, output)
        momentlist(input, output)
        return output
    }
    
    private func userInfo(_ input:LYQInput, _ output:LYQOutput) {
        input.userinfoCmd.subscribe(onNext: { [unowned self] _ in
            self.requestStatus.accept(.requesting)
            LYQNetwork.request(MultiTarget(TWServiceApi.userInfo))
                .mapModel(UserInfoModel.self)
                .subscribe(onNext: { [unowned self] (userInfo) in
                    if let userInfo = userInfo {
                        //cache user info
                        self.cacheUserInfo(info: userInfo)
                        output.userInfo.onNext(userInfo)
                    } else {
                        self.requestStatus.accept(.failed)
                        output.toastMsg.onNext(("userinfo wrong", 1.0, nil))
                    }
                }, onError: { (error) in
                    self.requestStatus.accept(.failed)
                    output.toastMsg.onNext((LYQErrorDescription(error), 1.0, nil))
                }).disposed(by:self.disposeBag)

        }).disposed(by: self.disposeBag)
    }

    private func momentlist(_ input:LYQInput, _ output:LYQOutput) {
            input.momelistCmd.subscribe(onNext: { [unowned self] _ in

                LYQNetwork.request(MultiTarget(TWServiceApi.momenList))
                    .mapArrayModel(FriendMomentModel.self)
                .subscribe(onNext: { [unowned self] (objList) in
                    guard let momeList = objList else {
                        output.toastMsg.onNext(("request failed", 1.0, nil))
                        return
                    }

                    if let momeArr = self.convertMoment(modelArr: momeList) {
                        self.cacheMomeResult(arr: momeArr)
                        output.listMomo.onNext(momeArr)
                    } else {
                        self.requestStatus.accept(.failed)
                        output.toastMsg.onNext(("request failed", 1.0, nil))
                    }

                }, onError: { [unowned self] (error) in
                    self.requestStatus.accept(.failed)
                    output.toastMsg.onNext((LYQErrorDescription(error), 1.0, nil))
                }).disposed(by:self.disposeBag)

            }).disposed(by: self.disposeBag)
        }
//MARK:- - moments list cache and load
    private func cacheMomeResult(arr:[FriendMomentModel]) {
        let json = arr.toJSONString()
        if let jsonStr = json {
            // defaultCache jsonstr
            //let jsonData = jsonStr.data(using: .utf8)
            UserDefaults.standard.set(jsonStr, forKey: KeyMomentCache)
            UserDefaults.standard.synchronize()
        }
    }

    func loadFriendMoments() -> [FriendMomentModel]? {
        if let jsonStr = UserDefaults.standard.string(forKey: KeyMomentCache) {
            if let modelArr = [FriendMomentModel].deserialize(from: jsonStr) {
                return self.convertMoment(modelArr: modelArr)
            }
        }
        return nil
    }
//MARK:- - user info cache and load
    private func cacheUserInfo(info:UserInfoModel) {
        let json = info.toJSONString()
        if let jsonStr = json {
            UserDefaults.standard.set(jsonStr, forKey: KeyUserInfoCache)
            UserDefaults.standard.synchronize()
        }
    }

    func loadUserInfo() -> UserInfoModel? {
        if let jsonStr = UserDefaults.standard.string(forKey: KeyUserInfoCache) {
            if let userInfoObj = UserInfoModel.deserialize(from: jsonStr) {
                return userInfoObj
            }
        }
        return nil
    }

    /// Convert [FriendMomentModel?] to [FriendMomentModel]?
    func convertMoment(modelArr:[FriendMomentModel?]) -> [FriendMomentModel]? {
        let resultArr = modelArr.compactMap({ (model) -> FriendMomentModel? in

            guard let model = model else {
                return nil
            }
            if model.comments == nil && model.content == nil && model.sender == nil && model.images == nil {
                return nil
            }
            return model
            })

        for (index,moment) in resultArr.enumerated() {
            moment.identifyStr = "indenty\(index+1)"
        }

        return resultArr
    }
}

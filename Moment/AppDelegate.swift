//
//  AppDelegate.swift
//  Moment
//
//  Created by knight on 2019/9/25.
//  Copyright © 2019 Goswift ChengDu Co.,Ltd. All rights reserved.
//

import UIKit
import FLEX

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window?.rootViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController()
        configFlex(show: true)
        return true
    }

    private func configFlex(show isShow: Bool) {
        let userDefault = UserDefaults.standard
        if let code = userDefault.string(forKey: "passcode_preference"), code == "goswift" {
            FLEXManager.shared().showExplorer()
        } else {
            FLEXManager.shared()?.hideExplorer()
        }
    }

}


//
//  ViewController.swift
//  Moment
//
//  Created by knight on 2019/9/25.
//  Copyright © 2019 Goswift ChengDu Co.,Ltd. All rights reserved.
//

import UIKit
import NSObject_Rx
import ESPullToRefresh
import RxSwift
import Toast_Swift

class ViewController: UITableViewController {

    @IBOutlet weak var profileImgView: UIImageView!

    @IBOutlet weak var avatarImgView: UIImageView!

    @IBOutlet weak var nickLabel: UILabel!

    @IBOutlet weak var usernameLabel: UILabel!

    private let viewModel = LYQMomeViewModel()
    private let input = LYQMomeViewModel.LYQInput()
    private var output:LYQMomeViewModel.LYQOutput!

    /// Current page
    var page = 1
    let pageSize = 5
    /// One section for user info, the other for moments
    let sectionNum = 2
    let userInfoCellHeight:CGFloat = 266
    /// The datasource of tableview
    var momeList:[FriendMomentModel] = []
    /// All the moments fetched from TWService
    var allMomeList:[FriendMomentModel] = []
    /// To cache cell height
    var heightDic: Dictionary<IndexPath, Any> = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        viewModelBind()
        configTableFresh()
        loadCache()
    }

    /// Config moment with page number, five rows a page
    /// - Parameter currentPage: first page  currentPage = 1
    /// - ([FriendMomentModel],Bool), Bool = true ,shown all the moments
    private func confMomentSource(currentPage:Int) -> ([FriendMomentModel],Bool) {
        let momeShownNum = currentPage * pageSize
        let totalMomeNum = self.allMomeList.count
        if momeShownNum < totalMomeNum {
            let obser = Observable.from(self.allMomeList)
            obser.take(momeShownNum).toArray().subscribe(onNext:{ [unowned self] momeArr in
                self.momeList = momeArr
            }).disposed(by:self.rx.disposeBag)

        } else {
            return (self.allMomeList,false)
        }
        return (momeList,true)
    }

    func updateUserInfo(model: UserInfoModel) {
        nickLabel.text = model.nick
        usernameLabel.text = model.username
        ImgCacheManager.instance.imageForUrl(urlString: model.avatar) { [unowned self] (img, url) in
            if let img = img {
                self.avatarImgView.image = img
            }
        }

        ImgCacheManager.instance.imageForUrl(urlString: model.profileImage) { [unowned self] (img, url) in
            if let img = img {
                self.profileImgView.image = img
            }
        }
    }

    fileprivate func configTableFresh() {

        self.tableView.es.addPullToRefresh { [unowned self] in
            self.input.userinfoCmd.onNext(())
            self.input.momelistCmd.onNext(())
        }

        self.tableView.es.addInfiniteScrolling { [unowned self] in
            self.page = self.page + 1
            let tuple = self.confMomentSource(currentPage: self.page)
            print("fresh - arr:\(tuple.0.count) \(self.momeList.count) - page :\(self.page)")
            if tuple.1 {
                self.momeList = tuple.0
                UIView.performWithoutAnimation {
                    self.tableView.reloadData()
                }
                DispatchQueue.main.asyncAfter(deadline: .now()+0.3, execute: {
                    self.tableView.es.stopLoadingMore()
                })
            } else {
                self.momeList = tuple.0
                UIView.performWithoutAnimation {
                    self.tableView.reloadData()
                }
                DispatchQueue.main.asyncAfter(deadline: .now()+0.3, execute: {
                    self.tableView.es.noticeNoMoreData()
                })
            }
        }
    }

    fileprivate func setupTable() {
        tableView.register(UINib(nibName: "MomeItemCell", bundle: Bundle.main), forCellReuseIdentifier: "MomeItemCell")
        tableView.contentInset = UIEdgeInsets(top: -44, left: 0, bottom: 0, right: 0)
    }

    fileprivate func viewModelBind() {
        output = viewModel.transform(input: input)
        input.userinfoCmd.onNext(())
        input.momelistCmd.onNext(())
        self.view.makeToastActivity(.center)
        output.userInfo.subscribe(onNext: { [unowned self] (model) in
            self.view.hideToastActivity()
            self.updateUserInfo(model: model)
        }).disposed(by:rx.disposeBag)

        output.listMomo.subscribe(onNext: { [unowned self] momeArr in
            self.view.hideToastActivity()
            self.allMomeList = momeArr
            self.page = 1
            if momeArr.count > 1 {
                _ = self.confMomentSource(currentPage: 1)
            }
            UIView.performWithoutAnimation {
                self.tableView.reloadData()
            }
            self.tableView.es.stopPullToRefresh()

        }).disposed(by: rx.disposeBag)

        output.toastMsg.subscribe(onNext:{ (msg,num,block) in
            self.view.hideToastActivity()
            self.view.makeToast(msg)
        }).disposed(by:rx.disposeBag)

        viewModel.requestStatus.subscribe(onNext:{ [unowned self] status in
            self.view.hideToast()
            if status == .failed || status == .noNetwork{
                self.view.makeToast("Request failed")
            }

        }).disposed(by:rx.disposeBag)
    }

    fileprivate func loadCache() {
        var loadCached = false
        //load cache
        if let cacheUserInfo = viewModel.loadUserInfo() {
            self.updateUserInfo(model: cacheUserInfo)
            loadCached = true
        }

        if let cachedMoments = viewModel.loadFriendMoments() {
            self.allMomeList = cachedMoments
            _ = self.confMomentSource(currentPage: 1)
            loadCached = true
        }
        if loadCached {
            self.view.hideToastActivity()
        } else {
            input.momelistCmd.onNext(())
        }
    }
}

//mark: implementation for tableview datasource
extension ViewController {

    override func numberOfSections(in tableView: UITableView) -> Int {
        return sectionNum
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return super.tableView(tableView, numberOfRowsInSection: section)
        } else {
            return self.momeList.count
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            return super.tableView(tableView, cellForRowAt: indexPath)
        } else {
            let momentCell = tableView.dequeueReusableCell(withIdentifier: "MomeItemCell") as! MomeItemCell
            guard let momeCell = momentCell.model, momeCell == momeList[indexPath.row] else {
                momentCell.model = momeList[indexPath.row]
                return momentCell
            }
            return momentCell
        }
    }

    override func tableView(_ tableView: UITableView, indentationLevelForRowAt indexPath: IndexPath) -> Int {
        if indexPath.section == 0 {
            return super.tableView(tableView, indentationLevelForRowAt: indexPath)
        } else {
            return super.tableView(tableView, indentationLevelForRowAt: IndexPath(row: 0, section: 1))
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 0 {
            return userInfoCellHeight
        } else {
            return self.tableView.rowHeight
        }

    }

    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = self.heightDic[indexPath] as? NSNumber
        if let h = height {
            return CGFloat(h.floatValue)
        } else {
            return UITableView.automaticDimension
        }
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let height = NSNumber(value: Float(cell.frame.size.height))
        self.heightDic[indexPath] = height
    }

}


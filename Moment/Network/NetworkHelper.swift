//
//  NetworkHelper.swift
//  InstallManage
//
//  Created by knight on 2018/1/17.
//  Copyright © 2018年 Goswift iOS team All rights reserved.
//

import Foundation
import UIKit

public var KLCAppType = "type"
public var KLCRuntimeEnvType = "Appstore"

public var MomentURL = "https://thoughtworks-mobile-2018.herokuapp.com"

public enum KLCRequstResultStatus {
/// 请求成功
    case success
    /// 请求失败
    case failed
    /// 没网
    case noNetwork
    /// 正在请求
    case requesting
    
    public var desc:String {
        switch self {
        case .failed:
            return "Request failed"
        case .noNetwork:
            return "no network"
        default:
            return ""
        }
    }
}

let kTicket = "kTicket"

public func getTicket() -> String? {
    return UserDefaults.standard.string(forKey: kTicket)
}

/// 客户端类型|操作系统|操作系统版本号|产品|客户端版本号|手机品牌|手机型号|屏幕分辨率|IMEI|渠道代码
public func klicenUserAgent() -> [String : String] {
    let phoneVersion = UIDevice.current.systemVersion
    let currentVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
    let platform = getPlatform()
    let screenSize = getScreen()
    let deviceUUID = (UIDevice.current.identifierForVendor?.uuidString)!// "readUDID()"
    let userAgent = "MOBILE|iOS|\(phoneVersion)|\(KLCAppType)|\(currentVersion)|iPhone|\(platform)|\(screenSize)|\(deviceUUID)|1"
    return ["Client-Agent" : userAgent, "X-TRACE-ID" : ""]
}

private func getPlatform() -> String {
    var size = 0
    sysctlbyname("hw.machine", nil, &size, nil, 0)
    var machine = [CChar](repeating: 0,  count: Int(size))
    sysctlbyname("hw.machine", &machine, &size, nil, 0)
    return String(cString: machine)
}

private func getScreen() -> String {
    let dotHeight = UIScreen.main.bounds.size.height
    let dotWidth = UIScreen.main.bounds.size.width
    let scale = UIScreen.main.scale
    let width = dotWidth * scale
    let height = dotHeight * scale
    return "\(width)*\(height)"
}


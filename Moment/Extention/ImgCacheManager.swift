//
//  CacheManager.swift
//  Moment
//
//  Created by knight on 2019/9/25.
//  Copyright © 2019 Goswift ChengDu Co.,Ltd. All rights reserved.
//

import UIKit

class ImgCacheManager {

    static let instance : ImgCacheManager = ImgCacheManager()
    // to cache the image data
    private var cache = NSCache<AnyObject, AnyObject>()

    lazy var appendCacheDir: NSString = {
        let home = NSHomeDirectory() as NSString
        let sstring = home.appendingPathComponent("Documents") as NSString
        return sstring
    }()

    func cachedImg(urlStr:String) -> UIImage? {
        let data: Data? = self.cache.object(forKey: urlStr as AnyObject) as? Data

        if let imgData = data {
            return UIImage(data: imgData as Data)
        }

        if let sanboxImg = sanboxCachedImg(urlString: urlStr) {
            return sanboxImg
        }
        return nil
    }

    func imageForUrl(urlString: String, completionHandler:@escaping (_ image: UIImage?, _ url: String) -> ()) {
        // check merroy and sanbox
        if let img = cachedImg(urlStr: urlString) {
            DispatchQueue.main.async {
                completionHandler(img, urlString)
            }
        } else {
            downloadImg(urlString: urlString, completionHandler: completionHandler)
        }
    }

    func sanboxCachedImg(urlString: String) -> UIImage? {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            guard let base64Name = urlString.encode64() else {
                print("sanbox base64Name error \(urlString)")
                return nil
            }

            let fileURL = dir.appendingPathComponent(base64Name)

            do {
                let sanboxData = try Data(contentsOf: fileURL)
                let image = UIImage(data: sanboxData)
                self.cache.setObject(sanboxData as AnyObject, forKey: urlString as AnyObject)
                return image
            } catch {
                return nil
            }
        }
        return nil
    }

    func downloadImg(urlString: String, completionHandler:@escaping (_ image: UIImage?, _ url: String) -> ()) {

        DispatchQueue.global().async {
            // user URLSession download the data
            let downloadTask: URLSessionDataTask = URLSession.shared.dataTask(with: URL(string: urlString)!, completionHandler: { (data, response, error) in
                if (error != nil) {
                    DispatchQueue.main.async {
                        completionHandler(nil, urlString)
                    }
                    return
                }
                if let imgData = data {
                    if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                        guard let base64Name = urlString.encode64() else {
                            let image = UIImage(data: imgData)
                            self.cache.setObject(data as AnyObject, forKey: urlString as AnyObject)
                            DispatchQueue.main.async {
                                completionHandler(image, urlString)
                            }
                            return
                        }

                        let fileURL = dir.appendingPathComponent(base64Name)

                        try? imgData.write(to: fileURL, options: .atomic)

                        let image = UIImage(data: imgData)
                        self.cache.setObject(data as AnyObject, forKey: urlString as AnyObject)
                        DispatchQueue.main.async {
                            completionHandler(image, urlString)
                        }
                    }
                }
            })
            downloadTask.resume()
        }
    }
}

//
//  UIView+Extension.swift
//  Moment
//
//  Created by knight on 2019/9/28.
//  Copyright © 2019 Goswift ChengDu Co.,Ltd. All rights reserved.
//

import UIKit

extension UIView {

    var currentViewController: UIViewController? {
        get {
            var n = self.next
            while n != nil {
                if let controller = n as? UIViewController {
                    return controller
                }
                n = n?.next
            }
            return nil
        }
    }
}

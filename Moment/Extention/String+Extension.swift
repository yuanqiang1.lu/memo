//
//  String+Extension.swift
//  Moment
//
//  Created by knight on 2019/9/30.
//  Copyright © 2019 Goswift ChengDu Co.,Ltd. All rights reserved.
//

import Foundation

extension String {
    func encode64() -> String? {
        let stringData = self.data(using: .utf8)
        if let data = stringData {
            return data.base64EncodedString()
        }
        return nil
    }

    func decode64() -> String? {
        if let data = Data(base64Encoded: self) {
            return String(data:data, encoding:.utf8)
        }
        return nil
    }
}
